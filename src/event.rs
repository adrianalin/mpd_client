use tokio::sync::{mpsc, oneshot};
use tokio::time::{timeout, Duration};
use tracing::debug;

#[derive(Debug)]
pub enum EventRequestType {
    Nop,
    Shutdown,
}
pub struct EventResult {}

pub struct EventRequest {
    pub req: EventRequestType,
    pub tx: oneshot::Sender<EventResult>,
}

pub type EventSender = mpsc::Sender<EventRequest>;

pub fn new_request() -> (EventRequest, oneshot::Receiver<EventResult>) {
    let (tx, rx) = oneshot::channel::<EventResult>();

    (
        EventRequest {
            req: EventRequestType::Nop,
            tx,
        },
        rx,
    )
}

pub const EVENT_WAIT_TIMEOUT: Duration = Duration::from_millis(1000);

pub async fn event(rx: &mut mpsc::Receiver<EventRequest>) -> Option<EventRequest> {
    if let Ok(r) = timeout(EVENT_WAIT_TIMEOUT, rx.recv()).await {
        if let Some(recv) = r {
            debug!("recv [{:?}]", recv.req);
            return Some(recv);
        }
    }

    None
}

pub async fn event_shutdown(rx: &mut mpsc::Receiver<EventRequest>) -> bool {
    if let Some(recv) = event(rx).await {
        if let EventRequestType::Shutdown = recv.req {
            recv.tx.send(EventResult {}).ok();
            return true;
        }
    }

    false
}
