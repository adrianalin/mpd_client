use std::io;
use std::pin::Pin;
use std::task::{Context, Poll};
use tokio::fs::File;
use tokio::io::{AsyncRead, AsyncSeekExt, ReadBuf};

pub struct FileRangeRead {
    file: Pin<Box<File>>,
    pub len: u64,
    cur: u64,
}

impl FileRangeRead {
    pub async fn new(mut file: tokio::fs::File, start: u64, end: u64) -> io::Result<Self> {
        if let io::Result::Err(x) = file.seek(std::io::SeekFrom::Start(start)).await {
            return io::Result::Err(x);
        }

        return io::Result::Ok(Self {
            file: Box::pin(file),
            len: end - start,
            cur: 0,
        });
    }
}

impl AsyncRead for FileRangeRead {
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        dst: &mut ReadBuf<'_>,
    ) -> Poll<std::io::Result<()>> {
        if self.cur >= self.len {
            Poll::Ready(Ok(()))
        } else {
            match self.file.as_mut().poll_read(cx, dst) {
                Poll::Pending => Poll::Pending,
                Poll::Ready(x) => match x {
                    Err(e) => Poll::Ready(Err(e)),
                    Ok(()) => {
                        let mut n = dst.filled().len() as u64;
                        if self.cur + n >= self.len {
                            n = self.len - self.cur;
                        }

                        self.cur += n;
                        Poll::Ready(Ok(()))
                    }
                },
            }
        }
    }
}
