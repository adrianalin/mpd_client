use std::net::SocketAddr;
use std::str::FromStr;

use tokio::io::{AsyncBufReadExt, AsyncWriteExt, BufReader};
use tokio::net::TcpStream;

use tokio::sync::oneshot;
use tokio::time::{timeout, Duration};
use tracing::{debug, error, info, warn};

use serde::{Deserialize, Serialize};

use crate::context;

const EVENT_WAIT_TIMEOUT: Duration = Duration::from_millis(2000);

pub enum MpdComRequestType {
    Nop,
    Cmd(String),
    SetVol(String),
    CmdInner(String),
    SetMute(String),
    AddUrl(String, String),
    AddAuxIn(String, String),
    TestSound,
    Shutdown,
}

pub struct MpdComRequest {
    pub req: MpdComRequestType,
    pub tx: oneshot::Sender<MpdComResult>,
}

impl MpdComRequest {
    pub fn new() -> (MpdComRequest, oneshot::Receiver<MpdComResult>) {
        let (tx, rx) = oneshot::channel::<MpdComResult>();

        (
            MpdComRequest {
                req: MpdComRequestType::Nop,
                tx,
            },
            rx,
        )
    }
}

pub fn quote_arg_f(arg: &str) -> String {
    String::from("\"") + &arg.replace('\\', r"\\").replace('"', r#"\""#) + "\""
}

pub fn quote_arg(arg: &str) -> String {
    let mut arg = arg.replace('\\', r"\\").replace('"', r#"\""#);

    if arg.contains(' ') {
        arg = String::from("\"") + &arg + "\""
    }

    arg
}

#[derive(Debug, Serialize, Clone)]
pub struct MpdComOkStatus {
    pub status: Vec<(String, String)>,
}

impl MpdComOkStatus {
    pub fn from(f: MpdComOk) -> MpdComOkStatus {
        MpdComOkStatus { status: f.flds }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct MpdComOk {
    pub flds: Vec<(String, String)>,
    pub bin: Option<Vec<u8>>,
}

impl MpdComOk {
    pub fn new() -> MpdComOk {
        MpdComOk {
            flds: Vec::new(),
            bin: None,
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct MpdComErr {
    pub err_code: i32,
    pub cmd_index: i32,
    pub cur_cmd: String,
    pub msg_text: String,
}

impl MpdComErr {
    pub fn new(err_code: i32) -> MpdComErr {
        MpdComErr {
            err_code,
            cmd_index: 0,
            cur_cmd: String::new(),
            msg_text: String::new(),
        }
    }
}

pub type MpdComResult = Result<MpdComOk, MpdComErr>;
pub type MpdComStatusResult = Result<MpdComOkStatus, MpdComErr>;

struct MpdcomExec {
    addr: SocketAddr,
    conn: Option<TcpStream>,
}

impl MpdcomExec {
    async fn exec(&mut self, cmd: String, protolog: bool) -> std::io::Result<MpdComResult> {
        if protolog {
            debug!("> {}", cmd);
        }

        let conn = self.conn.as_mut().unwrap();
        conn.write(cmd.as_bytes()).await?;
        conn.write(&[0x0A]).await?;
        conn.flush().await?;

        let mut is_ok = false;
        let mut ret_ok = MpdComOk::new();
        let ret_err = MpdComErr::new(-1);

        let mut reader = BufReader::new(conn);
        let mut buf = String::new();

        'outer: loop {
            buf.clear();
            if let Ok(x) = reader.read_line(&mut buf).await {
                if x == 0 {
                    break 'outer;
                }
            }

            if protolog {
                debug!("< {}", buf);
            }

            if buf == "OK\n" {
                is_ok = true;
                break 'outer;
            } else if buf.starts_with("ACK [") {
            } else {
                let re = regex::Regex::new(r"^([^:]*):\s*(.*)\n").unwrap();
                if let Some(x) = re.captures(&buf) {
                    if &x[1] == "binary" {
                        // let _binlen = x[2].parse().unwrap();
                    } else {
                        ret_ok
                            .flds
                            .push((String::from(x[1].trim()), String::from(x[2].trim())));
                    }
                }
            }
        }

        Ok(if is_ok { Ok(ret_ok) } else { Err(ret_err) })
    }
}

pub async fn mpdcom_task(
    arwlctx: context::ARWLContext,
    adr: SocketAddr,
    mut mpdcom_rx: tokio::sync::mpsc::Receiver<MpdComRequest>,
) {
    let mut mpd_exec = MpdcomExec {
        addr: adr,
        conn: None,
    };
    let mpd_protolog = false;

    info!("mpdcom {:?} protolog {:?}", mpd_exec.addr, mpd_protolog);

    loop {
        if mpd_exec.conn.is_none() {
            match TcpStream::connect(mpd_exec.addr).await {
                Ok(mut x) => {
                    info!("Connection succeeded!");
                    let mut reader = BufReader::new(&mut x);
                    let mut buf = String::new();
                    let _ = reader.read_line(&mut buf).await;

                    info!("Connected {}", buf);
                    if !buf.starts_with("OK MPD") {
                        warn!("Connection failed. Shutting down!");
                        let _ = x.shutdown().await;
                    } else {
                        mpd_exec.conn = Some(x);
                    }
                }
                Err(x) => {
                    warn!("Connect error {:?}!", x);
                    continue;
                }
            }
        }

        if mpd_exec.conn.is_some() {
            match mpd_exec.exec(String::from("status"), mpd_protolog).await {
                Ok(mut x) => {
                    // if let Ok(x2) = x.as_mut() {
                    //     {
                    //         let ctx = arwlctx.read().await;

                    //         x2.flds
                    //             .push((String::from("_x_time"), chrono::Local::now().to_rfc3339()));
                    //         x2.flds
                    //             .push((String::from("_x_product"), String::from(&ctx.product)));
                    //         x2.flds
                    //             .push((String::from("_x_version"), String::from(&ctx.version)));
                    //         x2.flds.push((
                    //             String::from("_x_ws_status_intv"),
                    //             format!("{:?}", &ctx.ws_status_intv),
                    //         ));
                    //         x2.flds.push((
                    //             String::from("_x_ws_data_intv"),
                    //             format!("{:?}", &ctx.ws_data_intv),
                    //         ));
                    //     }

                    //     let p = x2.flds.iter().position(|x| x.0 == "volume");

                    //     if let Some(p) = p {
                    //         let vol = x2.flds.remove(p);

                    //         let volval = if let Ok(volval) = u8::from_str(&vol.1) {
                    //             volval
                    //         } else {
                    //             0
                    //         };

                    //         let mut ctx = arwlctx.write().await;

                    //         if !ctx.mpd_mute || volval > 0 {
                    //             ctx.mpd_volume = volval;
                    //             ctx.mpd_mute = false;
                    //         }

                    //         x2.flds
                    //             .push((String::from("volume"), ctx.mpd_volume.to_string()));
                    //         x2.flds.push((
                    //             String::from("mute"),
                    //             String::from(if ctx.mpd_mute { "1" } else { "0" }),
                    //         ));
                    //         x2.flds.push((String::from("_x_volume"), vol.1));
                    //     }
                    // }

                    let mut songids = Vec::<String>::new();

                    if let Ok(x) = x.as_ref() {
                        for (k, v) in x.flds.iter() {
                            if k == "songid" {
                                songids.push(String::from(v));
                            }
                        }
                    }

                    if let Ok(x2) = x.as_mut() {
                        for si in songids {
                            if let Ok(Ok(x1)) =
                                mpd_exec.exec(format!("playlistid {}", si), false).await
                            {
                                x2.flds.extend_from_slice(&x1.flds);
                            }
                        }
                    }

                    let mut ctx = arwlctx.write().await;
                    ctx.mpd_status_json = match x {
                        Ok(x2) => {
                            match serde_json::to_string(&MpdComStatusResult::Ok(
                                MpdComOkStatus::from(x2),
                            )) {
                                Ok(x) => x,
                                _ => String::new(),
                            }
                        }
                        Err(ref x2) => match serde_json::to_string(x2) {
                            Ok(x) => x,
                            _ => String::new(),
                        },
                    };
                }

                Err(x) => {
                    warn!("Connection error [{:?}]", x);
                    let _res = mpd_exec.conn.as_mut().unwrap().shutdown().await;
                    mpd_exec.conn = None;

                    let mut ctx = arwlctx.write().await;
                    ctx.mpd_status_json =
                        match serde_json::to_string(&MpdComResult::Err(MpdComErr::new(-1))) {
                            Ok(x) => x,
                            _ => String::new(),
                        };
                    continue;
                }
            }
        }

        if let Ok(recv) = timeout(EVENT_WAIT_TIMEOUT, mpdcom_rx.recv()).await {
            let recv = recv.unwrap();
            match recv.req {
                MpdComRequestType::Shutdown => {
                    if mpd_exec.conn.is_some() {
                        info!("connection close");
                        let res = mpd_exec.conn.as_mut().unwrap().shutdown().await;
                        match res {
                            Ok(x) => {}
                            Err(x) => {
                                error!("mpd_exec shutdown failed: {}", x);
                            }
                        }
                    }
                    // recv.tx.send(Ok(MpdComOk::new())).ok(); whyy
                    break;
                }
                MpdComRequestType::SetVol(volume) => {
                    debug!("Set vol {}", volume);
                    if let Ok(vol) = u8::from_str(&volume) {
                        if vol <= 100 {
                            let mut done = false;

                            {
                                let mut ctx = arwlctx.write().await;

                                if ctx.mpd_mute {
                                    ctx.mpd_volume = vol;
                                    done = true;
                                }
                            }

                            if !done {
                                let cmd = String::from("setvol ") + &vol.to_string();

                                match mpd_exec.exec(cmd, mpd_protolog).await {
                                    Ok(x) => {
                                        recv.tx.send(x).ok();
                                    }
                                    Err(x) => {
                                        warn!("connection error [{:?}]", x);
                                        mpd_exec.conn.as_mut().unwrap().shutdown().await;
                                        mpd_exec.conn = None;

                                        recv.tx.send(Err(MpdComErr::new(-2))).ok();
                                    }
                                }
                            } else {
                                recv.tx.send(Ok(MpdComOk::new())).ok();
                            }
                        } else {
                            warn!("Volume out of range: {}", volume);
                            recv.tx.send(Err(MpdComErr::new(-3))).ok();
                        }
                    } else {
                        recv.tx.send(Err(MpdComErr::new(-3))).ok();
                    }
                }
                MpdComRequestType::SetMute(mute) => {
                    debug!("Set mute {:?}", mute);

                    let mute = matches!(mute.to_lowercase().as_str(), "1" | "true" | "on");

                    let cmd = if mute {
                        String::from("setvol 0")
                    } else {
                        let ctx = arwlctx.read().await;
                        String::from("setvol ") + &ctx.mpd_volume.to_string()
                    };

                    match mpd_exec.exec(cmd, mpd_protolog).await {
                        Ok(x) => {
                            let mut ctx = arwlctx.write().await;
                            ctx.mpd_mute = mute;

                            recv.tx.send(x).ok();
                        }
                        Err(x) => {
                            warn!("connection error [{:?}]", x);
                            mpd_exec.conn.as_mut().unwrap().shutdown().await;
                            mpd_exec.conn = None;
                            recv.tx.send(Err(MpdComErr::new(-2))).ok();
                        }
                    }
                }
                MpdComRequestType::AddUrl(url, append) => {
                    debug!("Add url {} {}", url, append);
                }
                MpdComRequestType::AddAuxIn(url, name) => {
                    debug!("AddAuxIn {} {}", url, name);
                }
                MpdComRequestType::TestSound => {
                    debug!("TestSound");
                    let testsound_urllist = { arwlctx.read().await.testsound_urllist() };

                    let mut ret_ok = MpdComOk::new();
                    let mut ret_err: Option<MpdComErr> = None;

                    for (url, title) in testsound_urllist {
                        let cmd = String::from("addid ") + &quote_arg(&url);

                        debug!("testsound {:?}", cmd);

                        match mpd_exec.exec(cmd, mpd_protolog).await {
                            Ok(x) => match x {
                                Ok(mut x) => {
                                    let id = x
                                        .flds
                                        .iter()
                                        .find(|x| x.0 == "Id")
                                        .map(|x| String::from(&x.1));

                                    ret_ok.flds.push((String::from("file"), String::from(&url)));
                                    ret_ok.flds.append(&mut x.flds);

                                    if !title.is_empty() {
                                        if let Some(id) = id {
                                            let cmd = String::from("addtagid ")
                                                + &quote_arg(&id)
                                                + r#" "Title" "#
                                                + &quote_arg_f(&title);

                                            debug!("testsound [{}]", &cmd);

                                            match mpd_exec.exec(cmd, mpd_protolog).await {
                                                Ok(x) => match x {
                                                    Ok(_) => {
                                                        ret_ok.flds.push((
                                                            String::from("Title"),
                                                            String::from(&title),
                                                        ));
                                                    }
                                                    Err(x) => {
                                                        warn!("error [{:?}]", x);
                                                        ret_err = Some(x);
                                                    }
                                                },
                                                Err(x) => {
                                                    warn!("connection error [{:?}]", x);
                                                    mpd_exec
                                                        .conn
                                                        .as_mut()
                                                        .unwrap()
                                                        .shutdown()
                                                        .await;
                                                    mpd_exec.conn = None;
                                                    // conn_try_time = Some(Instant::now());
                                                }
                                            }
                                        }
                                    }
                                }
                                Err(x) => {
                                    warn!("error [{:?}]", x);
                                    ret_err = Some(x);
                                    break;
                                }
                            },
                            Err(x) => {
                                warn!("connection error [{:?}]", x);
                                mpd_exec.conn.as_mut().unwrap().shutdown().await;
                                mpd_exec.conn = None;
                                // conn_try_time = Some(Instant::now());
                            }
                        }
                    }

                    if let Some(x) = ret_err {
                        recv.tx.send(Err(x)).ok();
                    } else {
                        recv.tx.send(Ok(ret_ok)).ok();
                    }
                }
                MpdComRequestType::Cmd(cmd) => {
                    debug!("Cmd {}", cmd);
                    if cmd != "close" && mpd_exec.conn.is_some() {
                        match mpd_exec.exec(cmd, mpd_protolog).await {
                            Ok(x) => {
                                recv.tx.send(x).ok();
                            }
                            Err(x) => {
                                warn!("connection error [{:?}]", x);
                                mpd_exec.conn.as_mut().unwrap().shutdown().await;
                                mpd_exec.conn = None;

                                recv.tx.send(Err(MpdComErr::new(-2))).ok();
                            }
                        }
                    } else {
                        warn!("cmd error -2");
                        recv.tx.send(Err(MpdComErr::new(-2))).ok();
                    }
                }
                MpdComRequestType::CmdInner(_) => {}
                MpdComRequestType::Nop => {}
            }
        }
    }

    info!("mpdcom stop.");
}
