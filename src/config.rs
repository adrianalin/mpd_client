use serde::Deserialize;
use std::fs;
use std::io;
use std::net::SocketAddr;
use tracing::{debug, error};

#[derive(Deserialize)]
pub struct Config {
    bind_addr: String,
    mpd_addr: String,
    pub mpd_protolog: bool,
    pub contents_dir: String,
}

impl Config {
    pub fn load_config() -> Option<Config> {
        let mut targets = vec![
            String::from("server.conf"),
            String::from("/etc/server.conf"),
        ];

        let args: Vec<String> = std::env::args().collect();
        if args.len() >= 2 {
            targets.insert(0, args[1].clone());
        }

        for t in targets {
            debug!("config try loading [{:?}]", &t);

            match fs::File::open(&t).map(io::BufReader::new) {
                Ok(f) => match serde_json::from_reader(f) {
                    Ok::<Config, _>(conf) => return Some(conf),
                    Err(e) => {
                        error!("Failed to deserialize config file: {:?}", e);
                    }
                },
                Err(e) => {
                    error!("Failed to read config file: {:?}", e);
                }
            }
        }

        None
    }

    pub fn bind_addr(&self) -> SocketAddr {
        self.bind_addr
            .parse::<SocketAddr>()
            .expect("Failed to parse bind_addr!")
    }

    pub fn mpd_addr(&self) -> SocketAddr {
        self.mpd_addr
            .parse::<SocketAddr>()
            .expect("Failed to parse mpd_addr!")
    }
}
