use color_eyre::{eyre::eyre, Report};
use std::sync::Arc;

use tokio::{join, sync, task};

use tracing::{debug, info};
use tracing_subscriber::EnvFilter;

mod bt;
mod btctrl;
mod config;
mod context;
mod event;
mod iolist;
mod mpdcom;

mod webserver;

#[tokio::main]
async fn main() -> Result<(), Report> {
    setup()?;

    let conf = match config::Config::load_config() {
        Some(conf) => conf,
        None => return Err(eyre!("Failed to load config file!")),
    };

    let (mpdcom_tx, mpdcom_rx) = sync::mpsc::channel::<mpdcom::MpdComRequest>(128);
    let (btctrl_tx, btctrl_rx) = sync::mpsc::channel::<btctrl::BtctrlRequest>(128);
    let (bt_agent_io_tx, bt_agent_io_rx) = sync::mpsc::channel::<btctrl::BtctrlRepryType>(16);

    let arwlctx = Arc::new(sync::RwLock::new(context::Context::new(
        mpdcom_tx,
        btctrl_tx,
        bt_agent_io_tx,
    )));

    let h_btctrl: task::JoinHandle<_> = task::spawn(btctrl::btctrl_task(
        arwlctx.clone(),
        btctrl_rx,
        bt_agent_io_rx,
    ));

    let (iolist_tx, iolist_rx) = sync::mpsc::channel::<event::EventRequest>(4);
    let h_iolist: task::JoinHandle<_> =
        task::spawn(iolist::iolist_task(arwlctx.clone(), iolist_rx));

    let h_mpdcom = task::spawn(mpdcom::mpdcom_task(
        arwlctx.clone(),
        conf.mpd_addr(),
        mpdcom_rx,
    ));

    webserver::run(conf.bind_addr(), conf.contents_dir, &arwlctx).await;
    info!("webserver shutdown.");

    {
        let mut ctx = arwlctx.write().await;
        // debug!("ws count {}", ctx.ws_sessions.len());

        // for (id, wss) in ctx.ws_sessions.iter_mut() {
        //     let (mut req, _) = event::new_request();
        //     info!("--closing wss: {}", id);
        //     // rxvec.push((rx, String::from(&wss.ws_name)));
        //     req.req = event::EventRequestType::Shutdown;
        //     // let _ = wss.session_tx.send(req).await;
        // }

        if ctx.bt_agent_io_rx_opend {
            let _ = ctx
                .bt_agent_io_tx
                .send(btctrl::BtctrlRepryType::Shutdown)
                .await;
        }
    }

    let (mut req, _) = event::new_request();
    req.req = event::EventRequestType::Shutdown;
    let _ = iolist_tx.send(req).await;
    let _ = join!(h_iolist);
    info!("iolist_task shutdown.");

    let (mut req, _) = btctrl::BtctrlRequest::new();
    req.req = btctrl::BtctrlRequestType::Shutdown;
    let _ = arwlctx.write().await.btctrl_tx.send(req).await;
    let _ = join!(h_btctrl);
    info!("btctrl_task shutdown.");

    let (mut req, _) = mpdcom::MpdComRequest::new();
    req.req = mpdcom::MpdComRequestType::Shutdown;
    let _ = arwlctx.write().await.mpdcom_tx.send(req).await;
    let _ = join!(h_mpdcom);
    info!("mpdcom_task shutdown.");

    Ok(())
}

fn setup() -> Result<(), Report> {
    if std::env::var("RUST_LIB_BACKTRACE").is_err() {
        std::env::set_var("RUST_LIB_BACKTRACE", "1")
    }
    if std::env::var("RUST_BACKTRACE").is_err() {
        std::env::set_var("RUST_BACKTRACE", "full")
    }
    color_eyre::install()?;

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "debug")
    }
    tracing_subscriber::fmt::fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .init();

    Ok(())
}
