use crate::btctrl;
use crate::mpdcom;
use rand::prelude::SliceRandom;
use rand::prelude::StdRng;
use rand::thread_rng;
use rand::SeedableRng;
use std::fs;
use std::path::PathBuf;
use std::sync::Arc;
use tokio::sync;
use tokio::time;
use tracing::error;

const TESTSOUND_DIR: &str = "tsource";
const TESTSOUND_NAME: &str = r"^test-\d+-\d-\d+-\d+s-(.+).mp3";

pub const ALSA_SOURCE_PROTO: &str = "asource://";
pub const ALSA_SINK_PROTO: &str = "asink://";

pub type ARWLContext = Arc<tokio::sync::RwLock<Context>>;

type UrlTitleList = Vec<(String, String)>;

pub struct Context {
    pub mpdcom_tx: sync::mpsc::Sender<mpdcom::MpdComRequest>,

    pub mpd_mute: bool,
    pub mpd_volume: u8,

    pub btctrl_tx: sync::mpsc::Sender<btctrl::BtctrlRequest>,

    pub mpd_status_json: String,
    pub bt_status_json: String,
    pub bt_notice_json: String,

    pub notice_reply_token: String,
    pub notice_reply_token_time: time::Instant,
    pub bt_agent_io_rx_opend: bool,
    pub bt_agent_io_tx: sync::mpsc::Sender<btctrl::BtctrlRepryType>,

    pub io_list_json: String,
    pub shutdown: bool,
    pub rng: StdRng,
}

impl Context {
    pub fn new(
        mpdcom_tx: sync::mpsc::Sender<mpdcom::MpdComRequest>,
        btctrl_tx: sync::mpsc::Sender<btctrl::BtctrlRequest>,
        bt_agent_io_tx: sync::mpsc::Sender<btctrl::BtctrlRepryType>,
    ) -> Context {
        Context {
            mpdcom_tx,
            mpd_mute: false,
            mpd_volume: 0,
            btctrl_tx,
            mpd_status_json: String::new(),
            bt_status_json: String::new(),
            bt_notice_json: String::new(),
            notice_reply_token: String::new(),
            notice_reply_token_time: time::Instant::now(),
            bt_agent_io_rx_opend: false,
            bt_agent_io_tx,
            io_list_json: String::new(),
            shutdown: false,
            rng: SeedableRng::from_rng(thread_rng()).unwrap(),
        }
    }

    pub fn testsound_urllist(&self) -> UrlTitleList {
        let mut path = PathBuf::from("/home/miha/rust_work/mpd_client/contents/");
        path.push(TESTSOUND_DIR);

        let mut ts = UrlTitleList::new();

        if let Ok(entries) = fs::read_dir(path) {
            for entry in entries {
                if let Ok(entry) = entry {
                    if let Ok(entry_fn) = entry.file_name().into_string() {
                        let RE: regex::Regex = regex::Regex::new(TESTSOUND_NAME).unwrap();
                        if RE.is_match(&entry_fn) {
                            let mut title = String::new();

                            match mp3_metadata::read_from_file(entry.path()) {
                                Ok(mp3md) => {
                                    if let Some(x) =
                                        mp3md.optional_info.iter().rev().find(|x| x.title.is_some())
                                    {
                                        if let Some(ref x) = x.title {
                                            title = String::from(x);
                                        }
                                    }

                                    if title == "" {
                                        if let Some(x) = mp3md.tag {
                                            title = x.title;
                                        }
                                    } else {
                                        title = String::from(
                                            title.trim_matches(|c| c == ' ' || c == '\u{0}'),
                                        );
                                    }
                                }
                                Err(x) => {
                                    error!("mp3_metadata error {:?}", x);
                                }
                            }

                            // TODO fix URL ip address
                            let entry = (
                                format!(
                                    "{}{}/{}",
                                    "http://192.168.0.157:3030/", TESTSOUND_DIR, entry_fn
                                ),
                                title,
                            );

                            ts.push(entry);
                        }
                    }
                }
            }
        }

        ts.sort();
        ts
    }

    pub fn make_random_token(&mut self) -> String {
        let src = "0123456789abcdef".as_bytes();
        let sel: Vec<u8> = src.choose_multiple(&mut self.rng, 16).cloned().collect();
        sel.iter().map(|&s| s as char).collect::<String>()
    }

    pub fn next_notice_reply_token(&mut self) -> String {
        self.notice_reply_token = self.make_random_token();
        self.notice_reply_token_time = time::Instant::now();

        String::from(&self.notice_reply_token)
    }

    pub fn current_notice_reply_token(&self) -> (String, time::Duration) {
        (
            String::from(&self.notice_reply_token),
            self.notice_reply_token_time.elapsed(),
        )
    }
}
