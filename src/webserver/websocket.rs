use crate::context;
use crate::event;
use futures_util::stream::SplitSink;
use futures_util::stream::SplitStream;
use futures_util::{SinkExt, StreamExt};
use std::collections::HashMap;
use std::mem;
use std::net::SocketAddr;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;
use tokio::task;
use tracing::{debug, error};
use warp::{filters::ws::Message, filters::ws::WebSocket, Filter};

/// Our global unique connection counter.
// static NEXT_CONNECTION_ID: AtomicUsize = AtomicUsize::new(1);

struct WsSession {
    pub ws_tx: SplitSink<WebSocket, Message>,
}

pub struct WebsocketContext {
    ws_sessions: HashMap<u16, WsSession>,
    pub transmit_running: bool,
}

pub type ARWL_WSContext = Arc<tokio::sync::RwLock<WebsocketContext>>;

impl WebsocketContext {
    pub fn new() -> WebsocketContext {
        WebsocketContext {
            ws_sessions: HashMap::new(),
            transmit_running: false,
        }
    }
}

async fn websocket_receive(
    ws_context: ARWL_WSContext,
    mut ws_rx: SplitStream<WebSocket>,
    ws_no: u16,
) {
    while let Some(result) = ws_rx.next().await {
        match result {
            Ok(mes) => {
                debug!("websocket recv: {:?}, ws_no: {:?}", &mes, &ws_no);
            }
            Err(e) => {
                debug!("websocket recv err: {:?}, ws_no: {:?}", &e, &ws_no);
                break;
            }
        }
    }

    {
        let mut context = ws_context.write().await;
        if let Some(mut s) = context.ws_sessions.remove(&ws_no) {
            if let Err(res) = s.ws_tx.close().await {
                debug!("websocket ws_tx close err: {:?}, ws_no: {:?}", &res, &ws_no);
            } else {
                debug!("websocket ws_tx close ws_no: {:?}", &ws_no);
            }
        }
    }
}

async fn websocket_transmit(arwlctx: context::ARWLContext, ws_context: ARWL_WSContext) {
    loop {
        tokio::time::sleep(event::EVENT_WAIT_TIMEOUT).await;

        let bt_status_json;
        let mpd_status_json;
        let bt_notice_json;
        {
            let mut ctx = arwlctx.write().await;
            bt_status_json = mem::take(&mut ctx.bt_status_json);
            mpd_status_json = mem::take(&mut ctx.mpd_status_json);
            bt_notice_json = mem::take(&mut ctx.bt_notice_json);
        }

        {
            let ctx = &mut ws_context.write().await;
            debug!("websocket num sessions: {:?}", ctx.ws_sessions.len());
            ctx.transmit_running = !ctx.ws_sessions.is_empty();
            if !ctx.transmit_running {
                break;
            }

            for (id, session) in &mut ctx.ws_sessions.iter_mut() {
                debug!("sending message to ws id: {:?}", id);
                for msg in [&bt_status_json, &mpd_status_json, &bt_notice_json] {
                    if msg.is_empty() {
                        continue;
                    }
                    if let Err(x) = session.ws_tx.send(Message::text(msg)).await {
                        error!("websocket tx error: {:?} {:?}", &x, &id);
                        break;
                    }
                }
            }
        }
    }
    debug!("websocket transmit stop");
}

async fn ws_client_connected(
    arwlctx: context::ARWLContext,
    ws_context: ARWL_WSContext,
    ws: WebSocket,
    addr: Option<SocketAddr>,
) {
    //= NEXT_CONNECTION_ID.fetch_add(1, Ordering::Relaxed);
    let ws_no = if let Some(a) = addr { a.port() } else { 0 };

    let (ws_tx, ws_rx) = ws.split();
    task::spawn(websocket_receive(ws_context.clone(), ws_rx, ws_no));
    debug!("wss connected: {:?} {:?}", ws_no, addr);
    {
        let ctx = &mut ws_context.write().await;
        ctx.ws_sessions.insert(ws_no, WsSession { ws_tx });

        if ctx.transmit_running {
            return;
        }
    }
    websocket_transmit(arwlctx.clone(), ws_context.clone()).await;
}

pub async fn make_route(
    arwlctx: context::ARWLContext,
    ws_context: ARWL_WSContext,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    let arwlctx_clone_filter = warp::any().map(move || arwlctx.clone());
    let ws_context_clone_filter = warp::any().map(move || ws_context.clone());

    warp::path!("ws")
        .and(arwlctx_clone_filter)
        .and(ws_context_clone_filter)
        .and(warp::ws())
        .and(warp::addr::remote())
        .and(warp::path::end())
        .and_then(
            |arwlctx: context::ARWLContext,
             ws_context: ARWL_WSContext,
             ws: warp::ws::Ws,
             addr: Option<SocketAddr>| async move {
                if !arwlctx.read().await.shutdown {
                    Ok(ws.on_upgrade(move |ws: WebSocket| {
                        ws_client_connected(arwlctx, ws_context, ws, addr)
                    }))
                } else {
                    Err(warp::reject::not_found())
                }
            },
        )
}
