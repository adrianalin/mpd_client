use crate::context;
use crate::mpdcom;
use crate::webserver::utils;
use serde::Deserialize;
use tokio::sync;
use tracing::debug;
use warp::{filters, Filter, Reply};

#[derive(Debug, Deserialize)]
struct CmdParam {
    cmd: String,
    arg1: Option<String>,
    arg2: Option<String>,
    arg3: Option<String>,
}

impl CmdParam {
    fn to_request(
        &self,
    ) -> (
        mpdcom::MpdComRequest,
        sync::oneshot::Receiver<mpdcom::MpdComResult>,
    ) {
        let mut cmd = self.cmd.trim_end().to_lowercase();

        let reqval = match cmd.as_str() {
            "setvol" => {
                if self.arg1.is_some() {
                    mpdcom::MpdComRequestType::SetVol(String::from(
                        self.arg1.as_ref().unwrap().as_str(),
                    ))
                } else {
                    mpdcom::MpdComRequestType::Nop
                }
            }

            "setmute" => {
                if self.arg1.is_some() {
                    mpdcom::MpdComRequestType::SetMute(String::from(
                        self.arg1.as_ref().unwrap().as_str(),
                    ))
                } else {
                    mpdcom::MpdComRequestType::Nop
                }
            }

            "addurl" => {
                if self.arg1.is_some() {
                    let url = String::from(self.arg1.as_ref().unwrap().as_str());

                    let arg = if self.arg2.is_some() {
                        String::from(self.arg2.as_ref().unwrap().as_str())
                    } else {
                        String::new()
                    };

                    mpdcom::MpdComRequestType::AddUrl(url, arg)
                } else {
                    mpdcom::MpdComRequestType::Nop
                }
            }

            "addauxin" => {
                if self.arg1.is_some() {
                    let url = String::from(self.arg1.as_ref().unwrap().as_str());

                    let arg = if self.arg2.is_some() {
                        String::from(self.arg2.as_ref().unwrap().as_str())
                    } else {
                        String::new()
                    };

                    mpdcom::MpdComRequestType::AddAuxIn(url, arg)
                } else {
                    mpdcom::MpdComRequestType::Nop
                }
            }

            "testsound" => mpdcom::MpdComRequestType::TestSound,

            "" => mpdcom::MpdComRequestType::Nop,

            _ => {
                if self.arg1.is_some() {
                    if let Some(x) = self.arg1.as_ref() {
                        if x.trim() != "" {
                            cmd += " ";
                            cmd += &mpdcom::quote_arg(x);
                        }
                    }
                }

                if self.arg2.is_some() {
                    if let Some(x) = self.arg2.as_ref() {
                        if x.trim() != "" {
                            cmd += " ";
                            cmd += &mpdcom::quote_arg(x);
                        }
                    }
                }

                if self.arg3.is_some() {
                    if let Some(x) = self.arg3.as_ref() {
                        if x.trim() != "" {
                            cmd += " ";
                            cmd += &mpdcom::quote_arg(x);
                        }
                    }
                }

                mpdcom::MpdComRequestType::Cmd(cmd)
            }
        };

        let (mut req, rx) = mpdcom::MpdComRequest::new();

        req.req = reqval;

        (req, rx)
    }
}

async fn cmd_response(arwlctx: context::ARWLContext, param: CmdParam) -> utils::RespResult {
    debug!("cmd <--- {:?}", &param);

    let (req, rx) = param.to_request();

    let _ = arwlctx.write().await.mpdcom_tx.send(req).await;

    Ok(match rx.await {
        Ok(x) => utils::json_response(&x),
        Err(x) => utils::internal_server_error(&format!("{:?}", x)),
    })
}

pub async fn make_route(arwlctx: context::ARWLContext) -> filters::BoxedFilter<(impl Reply,)> {
    let arwlctx_clone_filter = warp::any().map(move || arwlctx.clone());

    warp::path!("cmd")
        .and(arwlctx_clone_filter)
        .and(utils::make_route_getpost::<CmdParam>())
        .and_then(cmd_response)
        .boxed()
}
