use crate::context;
use std::{net::SocketAddr, sync::Arc};
use tokio::sync;
use tracing::info;

use warp::{filters, Filter, Reply};

mod bt_api;
mod mpd_api;
mod utils;
mod websocket;

pub async fn make_route(
    arwlctx: &context::ARWLContext,
    ws_context: &websocket::ARWL_WSContext,
    contents_path: String,
) -> filters::BoxedFilter<(impl Reply,)> {
    let r_ws = websocket::make_route(arwlctx.clone(), ws_context.clone()).await;
    let r_bt_cmd = bt_api::make_route(arwlctx.clone()).await;
    let r_cmd = mpd_api::make_route(arwlctx.clone()).await;
    let apis = r_ws.or(r_bt_cmd).or(r_cmd);

    // Static Content
    let content = warp::fs::dir(contents_path.clone());
    let root = warp::get()
        .and(warp::path::end())
        .and(warp::fs::file(format!("{}/index.html", contents_path)));
    let static_site = content.or(root);

    let routes = static_site.or(apis);
    // let with_log = warp::log("webserver");
    // routes.with(with_log).boxed()
    routes.boxed()
}

pub async fn run(bind_addr: SocketAddr, contents_path: String, arwlctx: &context::ARWLContext) {
    info!("bind: {:?}", bind_addr);
    info!("contents path: {:?}", contents_path);

    let shutdown = async {
        tokio::signal::ctrl_c()
            .await
            .expect("failed to install CTRL+C signal handler");
    };

    let ws_context = Arc::new(sync::RwLock::new(websocket::WebsocketContext::new()));

    let routes = make_route(arwlctx, &ws_context, contents_path).await;
    let (_, serving) = warp::serve(routes).bind_with_graceful_shutdown(bind_addr, shutdown);

    serving.await;

    {
        ws_context.write().await.transmit_running = false;
    }
}
