use crate::btctrl;
use crate::context;
use crate::webserver::utils;
use serde::Deserialize;
use tokio::sync;
use tracing::debug;
use warp::{filters, Filter, Reply};

#[derive(Debug, Deserialize)]
struct BtCmdParam {
    cmd: String,
    aid: String,
    did: String,
    sw: bool,
    arg: Option<String>,
}

impl BtCmdParam {
    fn to_request(
        &self,
    ) -> (
        btctrl::BtctrlRequest,
        sync::oneshot::Receiver<btctrl::BtctrlResult>,
    ) {
        let cmd = self.cmd.trim_end().to_lowercase();

        let (mut req, rx) = btctrl::BtctrlRequest::new();

        req.req = btctrl::BtctrlRequestType::Cmd(
            cmd,
            String::from(&self.aid),
            String::from(&self.did),
            self.sw,
            self.arg.as_ref().map(String::from),
        );

        (req, rx)
    }
}

async fn bt_cmd_response(arwlctx: context::ARWLContext, param: BtCmdParam) -> utils::RespResult {
    debug!("bt_cmd_response {:?}", &param);

    let (req, rx) = param.to_request();

    let _ = arwlctx.write().await.btctrl_tx.send(req).await;

    Ok(match rx.await {
        Ok(x) => utils::json_response(&x),
        Err(x) => utils::internal_server_error(&format!("{:?}", x)),
    })
}

pub async fn make_route(arwlctx: context::ARWLContext) -> filters::BoxedFilter<(impl Reply,)> {
    let arwlctx_clone_filter = warp::any().map(move || arwlctx.clone());

    warp::path!("bt_cmd")
        .and(arwlctx_clone_filter)
        .and(utils::make_route_getpost::<BtCmdParam>())
        .and_then(bt_cmd_response)
        .boxed()
}
