use serde::{de::DeserializeOwned, Serialize};
use tracing::debug;
use warp::http::header;
use warp::http::StatusCode;
use warp::{reject::Rejection, reply::Response, Filter};

pub type RespResult = Result<Response, Rejection>;

pub fn internal_server_error(t: &str) -> Response {
    let mut r = Response::new(String::from(t).into());
    *r.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
    r
}

pub fn make_route_getpost<T: DeserializeOwned + Send + 'static>(
) -> impl Filter<Extract = (T,), Error = Rejection> + Copy {
    warp::get()
        .and(warp::query::<T>())
        .or(warp::post()
            .and(
                warp::body::content_length_limit(1024 * 32), // Limit the body to 32kb...
            )
            .and(warp::body::form::<T>()))
        .unify()
}

pub fn json_response<T: ?Sized + Serialize>(t: &T) -> Response {
    let mut r = Response::new(
        match serde_json::to_string(t) {
            Ok(x) => {
                debug!("json response ---> {}", x);
                x
            }
            _ => String::new(),
        }
        .into(),
    );
    r.headers_mut().insert(
        header::CONTENT_TYPE,
        header::HeaderValue::from_str(&mime::APPLICATION_JSON.to_string()).unwrap(),
    );
    r
}
